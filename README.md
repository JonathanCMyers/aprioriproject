# README #

To run, please perform the following:

* cd apriori
* mvn package
* cp target/apriori-0.0.1-SNAPSHOT.jar $SPARK\_HOME/
* $SPARK\_HOME/bin/spark-submit --class {ChosenAlgorithmClassName} --master local[2] $SPARK\_HOME/apriori-0.0.1-SNAPSHOT.jar {InputFileName} {OutputFileName}

### What is this repository for? ###

This repository is for research conducted at Baylor University under the advisement of Dr. Greg Speegle.

### Who do I talk to? ###

* Jonathan Myers
* Jonathan\_Myers@baylor.edu