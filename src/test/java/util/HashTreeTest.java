package util;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class HashTreeTest {
	private Set<Integer> set1; // 1,2,3,4
	private Set<Integer> set2; // 1,2,3,5
	private Set<Integer> set3; // 2,3,4,5
	private Set<Integer> set4; // 1,2,4,6
	
	@Before
	public void setup() {
		set1 = new HashSet<Integer>(); // 1,2,3,4
		set1.add(1);
		set1.add(2);
		set1.add(3);
		set1.add(4);
		set2 = new HashSet<Integer>(); // 1,2,3,5
		set2.add(1);
		set2.add(2);
		set2.add(3);
		set2.add(5);
		set3 = new HashSet<Integer>(); // 2,3,4,5
		set3.add(2);
		set3.add(3);
		set3.add(4);
		set3.add(5);
		set4 = new HashSet<Integer>(); // 1,2,4,6
		set4.add(1);
		set4.add(2);
		set4.add(4);
		set4.add(6);
	}
	
	@Test
	public void test() {
		assertEquals(true, true);
	}
	
	@Test
	public void testHashTree1() {
		HashTree tree = new HashTree();
		
		tree.insert(set1);
		tree.insert(set2);
		tree.insert(set3);
		tree.insert(set4);
		
		Set<Integer> transaction = new HashSet<Integer>();
		transaction.add(1);
		transaction.add(2);
		transaction.add(3);
		transaction.add(4);
		transaction.add(5);
		
		Set<Set<Integer>> actualSubset = tree.subset(transaction);
		
		Set<Set<Integer>> correctSubset = new HashSet<Set<Integer>>();
		correctSubset.add(set1);
		correctSubset.add(set2);
		correctSubset.add(set3);

		assertEquals(correctSubset, actualSubset);
		
	}
	
	@Test
	public void testHashTree2() {
		HashTree tree = new HashTree();
		
		tree.insert(set1);
		tree.insert(set2);
		tree.insert(set3);
		tree.insert(set4);
		
		Set<Integer> transaction = new HashSet<Integer>();
		transaction.add(1);
		transaction.add(2);
		transaction.add(3);
		transaction.add(4);
		transaction.add(5);
		transaction.add(6);
		
		Set<Set<Integer>> actualSubset = tree.subset(transaction);
		
		Set<Set<Integer>> correctSubset = new HashSet<Set<Integer>>();
		correctSubset.add(set1);
		correctSubset.add(set2);
		correctSubset.add(set3);
		correctSubset.add(set4);

		assertEquals(correctSubset, actualSubset);
		
	}
}
