package apriori;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;
import util.AprioriUtil;

import java.io.File;
import java.util.Arrays;

import static com.google.common.base.Preconditions.checkArgument;

public class SPC {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SPC.class);
	private static final String CORRECT_ARGUMENTS = "Correct arguments: [input file name] [output file name] [desired itemset count] [support]";
	
	// Arguments:
		// 0: Input file name (in spark directory)
		// 1: Output file name
		// 2: Desired itemset count
		// 3: Support
	public static void main(String[] args) {
		if(args.length != 3) {
			System.err.println(CORRECT_ARGUMENTS);
			System.exit(1);
		}
		int itemsetCount = -1;
		int support = -1;
		try {
			itemsetCount = Integer.parseInt(args[2]);
			support = Integer.parseInt(args[3]);
		} catch(NumberFormatException e) {
			System.err.println(CORRECT_ARGUMENTS);
			System.exit(1);
		}
		File outputFile = new File(args[1]);
		new SPC().run(args[0], outputFile, itemsetCount, support);
	}
	
	public void run(String inputFileName, File outputFile, int itemsetCount, int support) {
		
		String master = "local[*]";
		
		SparkConf conf = new SparkConf()
				.setAppName(SPC.class.getName())
				.setMaster(master);
		
		JavaSparkContext context = new JavaSparkContext(conf);
		
		JavaRDD<String> inputLines = context.textFile(inputFileName);
		JavaPairRDD<String, Integer> mappedItems = AprioriUtil.Phase1Map(inputLines, LOGGER);
		AprioriUtil.PhaseAllReduce(outputFile, support, mappedItems, LOGGER);
		context.close();
	}

	/*
  public void run(String inputFilePath) {
    String master = "local[*]";

    SparkConf conf = new SparkConf()
        .setAppName(WordCountTask.class.getName())
        .setMaster(master);
    JavaSparkContext context = new JavaSparkContext(conf);

    context.textFile(inputFilePath)
        .flatMap(text -> Arrays.asList(text.split(" ")).iterator())
        .mapToPair(word -> new Tuple2<>(word, 1))
        .reduceByKey((a, b) -> a + b)
        .foreach(result -> LOGGER.info(
            String.format("Word [%s] count [%d].", result._1(), result._2)));
  }
  */
}