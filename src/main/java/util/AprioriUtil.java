package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.Tuple2;

public class AprioriUtil {
	
	public static final int MOD_PARAMETER = 3;
	
	public static int hash(Set<Integer> itemset, int level) {
		return ((Integer) (itemset.toArray())[level]) % MOD_PARAMETER;
	}
	
	public static JavaPairRDD<String, Integer> Phase1Map(JavaRDD<String> inputLines, Logger logger) {
		return inputLines.flatMap(line -> Arrays.asList(line.split(" ")).iterator())
				.mapToPair(item -> new Tuple2<>(item, 1));
	}
	
	public static void PhaseAllReduce(File outputFile, int support, JavaPairRDD<String, Integer> mappedItemsets, Logger logger) {
		
		try(PrintWriter printer = new PrintWriter(outputFile)) {
			List<Tuple2<String, Integer>> collectedItems = mappedItemsets.reduceByKey((x, y) -> x + y).
					filter(x -> x._2() >= support)
					.collect();
			if(collectedItems.size() == 0) {
				logger.info("No itemsets to output.");
				logger.info("Exiting program.");
			}
			collectedItems.forEach(result -> {
				logger.info(result._1() + ": " + result._2());
				printer.write(result._1());
			});
		} catch (FileNotFoundException e) {
			System.err.println("Could not open the given output file location.");
			System.exit(1);
		}
		
	}
	
	/*
	 
	   def PhaseAllReduce(mappedItems: RDD[(String, Int)], support: Int, file: File) : Unit = {
    val printer = new PrintWriter(file)
    val rand = new scala.util.Random

    val outputRDD = mappedItems.reduceByKey((x, y) => x + y)
    var didOutput = false
    outputRDD.collect().foreach{case(k, v) =>
      if(v >= support) {
        System.out.println("{" + k + "}: " + v + "\n")
        printer.write("{" + k + "}: " + v + "\n")
        didOutput = true
      }
    }
    printer.close()
    if(!didOutput) {
      System.out.println("No itemsets to output.")
      System.out.println("Exiting program.")
      System.exit(1)
    }
	 
	 */
	
	
	/*
	 
	 
	   public void run(String inputFilePath) {
    String master = "local[*]";

    SparkConf conf = new SparkConf()
        .setAppName(WordCountTask.class.getName())
        .setMaster(master);
    JavaSparkContext context = new JavaSparkContext(conf);

    context.textFile(inputFilePath)
        .flatMap(text -> Arrays.asList(text.split(" ")).iterator())
        .mapToPair(word -> new Tuple2<>(word, 1))
        .reduceByKey((a, b) -> a + b)
        .foreach(result -> LOGGER.info(
            String.format("Word [%s] count [%d].", result._1(), result._2)));
  }
	 
	 
	 */
	
	/*
	  	def Phase1Map(sc: SparkContext, numMappers: Int, args: Array[String]) : RDD[(String, Int)] = {
			    val inputRDD = sc.textFile(args(0), numMappers)
			    // Split the input file by line and store each line as an element of the RDD
			    val inputLines = inputRDD.flatMap(x => x.split("\n"))
			    val inputItems = inputLines.flatMap(_.split(" ").toBuffer.remove(1).split(","))
			    val mappedItems = inputItems.map(x => (x, 1))
			    return mappedItems
			  }


		def Phase2Map(sc: SparkContext, numMappers: Int, file: File) : RDD[(String, Int)] = {
			    // Remove {} and support count from the L_k file
			    val inputRDD = sc.textFile(file.getAbsolutePath(), numMappers)
			    val inputItems = inputRDD.map(_.split("}").toBuffer.remove(0).substring(1).split(",").toSet) // produces RDD[Set[String]]

			    // Create candidate itemset
			    val inputCandidateItemset = CreateC_kFromL_kMinusOne(inputItems)

			    // Test candidate itemset against database -> increment each value by 1 whenever it's found in a transaction
			    val mappedItems = IncrementSupportOnCandidateItemset(inputCandidateItemset)

			    return mappedItems
			  }
	*/
	
	
}


