package util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class HashTree {
	
	public static final int DEFAULT_NODE_SIZE = 3;
	
	private HashTreeNode rootNode;
	private int nodeSize;
	
	public HashTree(int nodeSize) {
		rootNode = new HashTreeNode(0, nodeSize);
	}
	
	public HashTree() {
		rootNode = new HashTreeNode(0, DEFAULT_NODE_SIZE);
	}
	
	// Insert an itemset into the HashTree
	public void insert(Set<Integer> data) {
		rootNode.insert(data);
	}
	
	// Find the itemsets in this HashTree contained within this transaction
	public Set<Set<Integer>> subset(Set<Integer> transaction) {
		return rootNode.subset(transaction);
	}
	
	public int getNodeSize() {
		return nodeSize;
	}
	
	@Override
	public String toString() {
		return rootNode.toString();
	}
}

class HashTreeNode {
	
	private int level; // Level in the tree (important for determining hash function)
	private int nodeSize; // Size of the node (how many elements/nodes we can have)
	private List<Set<Integer>> data;
	private List<HashTreeNode> nodes;
	
	public HashTreeNode(int level, int nodeSize) {
		this.level = level;
		this.nodeSize = nodeSize;
		data = new ArrayList<Set<Integer>>(nodeSize);
		nodes = null;
	}
	public HashTreeNode(int level) {
		this(level, 3);
	}
	public int getLevel() {
		return level;
	}
	public int getSize() {
		return nodeSize;
	}
	public boolean isDataNode() {
		return nodes==null;
	}
	public boolean isHashNode() {
		return data==null;
	}
	public void insert(Set<Integer> itemset) {
		
		// Process as hash node if we are hash node
		if(isHashNode()) {
			nodes.get(AprioriUtil.hash(itemset, level)).insert(itemset);
		}
		// Process as data node if we are data node
		else if(isDataNode()) {
			
			// Check if the data node is full
			if(data.size() >= nodeSize) {
				
				// Convert to hash node
				nodes = new ArrayList<HashTreeNode>(nodeSize);
				
				// Add nodeSize new DataNodes to us
				for(int i = 0; i < nodeSize; i++) {
					nodes.add(new HashTreeNode(level+1, nodeSize));
				}
				
				// Send all of our data to the new data nodes (since we are now a hash node)
				for(Set<Integer> currentItemset : data) {
					nodes.get(AprioriUtil.hash(currentItemset, level)).insert(currentItemset);
				}
				
				nodes.get(AprioriUtil.hash(itemset, level)).insert(itemset);
				data = null;
			} 
			// If we are not at full capacity, add normally to our data node:
			else {
				data.add(itemset);
			}
		}
		
	}
	public Set<Set<Integer>> subset(Set<Integer> transaction) {
		
		Set<Set<Integer>> itemsets = new HashSet<Set<Integer>>();
		
		// Process as hash node if we are hash node
		if(isHashNode()) {
			for(HashTreeNode node : nodes) {
				itemsets.addAll(node.subset(transaction));
			}
		}
		// Otherwise, process as data node since we are data node
		else if(isDataNode()) {
			for(Set<Integer> itemset : data) {
				if(transaction.containsAll(itemset)) {
					itemsets.add(itemset);
				}
			}
		}
		
		return itemsets;
	}
	
	@Override
	public String toString() {
		String toString = "";
		for(int i = 0; i < level; i++) {
			toString += "\t";
		}
		if(isHashNode()) {
			toString += "Hash: \n";
			for(HashTreeNode node : nodes) {
				toString += node;
			}
		}
		else if(isDataNode()) {
			toString += "Data: ";
			for(Set<Integer> itemsets : data) {
				toString += "{";
				Iterator<Integer> items = itemsets.iterator();
				while(items.hasNext()) {
					toString += items.next() + " ";
				}
				toString += "} ";
			}
			toString += "\n";
		}
		return toString;
	}
}